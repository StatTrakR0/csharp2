﻿using System;

namespace LABA02
{
    class Program
    {
        delegate double F2Delegate(double x1, double x2);

        private static double Func(double x1, double x2)
        {
            double a, b, c, d, s;
            a = x1 + 2 * x2 + 9;
            b = a / 0.666;
            c = Math.Exp(b);
            d = Math.Cos(c);
            s = Math.Pow(d, 3);

            return s;

        }

        private static double Acc(double Sum, double y)
        {
            if (Sum == 0)
            {
                return y;
            }
            return Sum * y;
        }


        private static void EnterData(out double x1Min, out double x1Max, out double x2Min, out double x2Max, out double dx1, out double dx2)
        {
            Console.Write("Enter x1 min: ");
            x1Min = double.Parse(Console.ReadLine());

            Console.Write("Enter x2 min: ");
            x2Min = double.Parse(Console.ReadLine());

            Console.Write("Enter x1 max: ");
            x1Max = double.Parse(Console.ReadLine());

            Console.Write("Enter x2 max: ");
            x2Max = double.Parse(Console.ReadLine());

            Console.Write("Enter dx1: ");
            dx1 = double.Parse(Console.ReadLine());

            Console.Write("Enter dx2: ");
            dx2 = double.Parse(Console.ReadLine());

        }

        private static void Tabulate(double x1Min, double x1Max, double x2Min, double x2Max, double dx1, double dx2, double Sum, F2Delegate func, F2Delegate acc)
        {
            double x1 = x1Min;
            Console.WriteLine("Tabulate function");
            while (x1 <= x1Max)
            {
                double x2 = x2Min;
                while (x2 <= x2Max)
                {
                    
                    double y = func(x1, x2);
                    Console.WriteLine("x1={0:f3}\t\tx2={1:f3}\t\ty={2}", x1, x2, y);

                    if (x1 > x1Min && x1 < x1Max && y < 0)
                    {
                        Sum = acc(Sum, y);
                    }
                    x2 += dx2;
                }
                x1 += dx1;

            }
            Console.WriteLine("===================");
            if (acc != null)
            {
                Console.WriteLine("The sum of all negative values is:");
                Console.WriteLine(Sum);
                Console.WriteLine("================");
            }

        }

        static void Main(string[] args)
        {
            EnterData(out double x1Min, out double x1Max, out double x2Min, out double x2Max, out double dx1, out double dx2);
            Tabulate(x1Min, x1Max, x2Min, x2Max, dx1, dx2, 0, Func, Acc);
            Console.ReadKey();
        }
    }
}